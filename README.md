# Data Knowledge base ES

# Base de Conocimientos
En esta wiki almacenaremos todos los links importantes que puedan servir de referencia para proyectos presentes o futuros, además de servir de biblioteca para la educación en las áreas pertinentes a la analítica y el Big Data.

## Forma de leer esta página
La página se encuentra dividida en las siguientes secciones:

- **Ingeniería de datos:** Sección concerniente a todos los links que estén relacionados al tratamiento de datos e ingeniería de características.
- **Ciencia de datos:** Sección concerniente a todos los links que estén relacionados a la inteligencia artificial, el aprendizaje automático, y las matemáticas detrás de los algoritmos.
- **Proyectos de Analytics:** Ejemplos de proyectos y cómo todo se junta en casos prácticos.
- **Academia:** Páginas o elementos de la academia relacionada con el mundo de datos.
- **Gestión AA & Big Data:** Sobre gestión de proyectos concernientes a Data Science & Big Data.
- **Otros:** Páginas que no caen en ninguna de las otras categorías. Suelen ser páginas de interés general.

Aquellos links que estén relacionados a Data Engineering y Data Science a la vez se colocarán en ambas secciones, explicando qué cosas conciernen a tales áreas.

## Ingeniería de datos

- **[Hidden Technical Debt in Machine Learning Systems](https://papers.nips.cc/paper/5656-hidden-technical-debt-in-machine-learning-systems.pdf):**, explica cómo realizar la arquitectura de un sistema y cómo los sistemas se acoplan con los datos.

### Recolección de datos
- **[Data collection: the essential, but unloved, foundation of the data value chain](https://snowplowanalytics.com/blog/2017/01/16/data-collection-the-essential-but-unloved-foundation-of-the-data-value-chain/):** Sobre la importancia de la recolección de datos y su calidad para la cadena de valor de datos.
- **[Product analytics part one, data and digital products](https://snowplowanalytics.com/blog/2018/01/19/product-analytics-part-one-data-and-digital-products/):** Serie de articulos sobre Analytics y A/B testing.


### Cloud
- **[Google Cloud Developer Cheat Sheet](https://docs.google.com/spreadsheets/d/1OkFbizpnc_iyzcApqRrqsNtUVazKJDtCyH5vw3352xM/htmlview?usp=drive_open&ouid=116471976024911956616&sle=true):** Todos los links de interés para manejarse en GCP.
- **[Optimizing your Cloud Storage performance](https://cloud.google.com/blog/products/gcp/optimizing-your-cloud-storage-performance-google-cloud-performance-atlas):** Optimizar el performance de Google Storage.

### Tools

- **[Imagen de Docker para Apache Beam](https://beam.apache.org/contribute/docker-images/):** Imagenes en Docker (Apache Beam).
- **[Featuretools](https://www.featuretools.com/):** Framework de Python para ingeniería de características.
- **[Deep Feature Synthesis: How Automated Feature Engineering Works](https://www.featurelabs.com/blog/deep-feature-synthesis/):** Cómo hacer feature engineering automatizado.
- **[Dask](https://dask.org/):** Escala Python para que use paralelismo. Útil a la hora de realizar ETL en datasets o procesar archivos muy grandes.
- **[EuclidesDB](https://euclidesdb.readthedocs.io/en/latest/):** Base de datos diseñada especialmente para ML.
- **[Modin](https://rise.cs.berkeley.edu/blog/modin-pandas-on-ray-october-2018/):** Optimización de workflows en Pandas.
- **[BeamSQL](https://beam.apache.org/documentation/dsls/sql/overview/):** Procesar flujos de datos con Apache Beam mediante SQL.
- **[Bash hackers](http://wiki.bash-hackers.org):** Tools y Tips para trabajar de forma robusta con scripts bash.

### Ejemplos
- **[Running JanusGraph with BigTable](https://cloud.google.com/solutions/running-janusgraph-with-bigtable):** Base de datos en base a grafos sobre BigTable.
- **[Gremlin Graph Guide](https://kelvinlawrence.net/book/Gremlin-Graph-Guide.html):** Guía de uso de Gremlin, lenguaje de consulta de grafos.
- **[A Feature Selection Tool for Machine Learning in Python](https://towardsdatascience.com/a-feature-selection-tool-for-machine-learning-in-python-b64dd23710f0):** Librería de Python que simplifica el trabajo de selección de features.
- **[Curso de Docker](https://www.youtube.com/watch?v=1LRzlUoyZg4&list=PLn5IkU1ZhgiZl4EH7AFkqs-pqF6ZUz_iS):** Series de videos que explican el uso de Docker.
- **[Mender and Cloud IoT facilitate robust device update management](https://cloud.google.com/blog/products/iot-devices/mender-and-cloud-iot-facilitate-robust-device-update-management):** Integración entre Mender y Google Cloud IoT.
- **[Avoiding SQL Anti-Patterns](https://cloud.google.com/bigquery/docs/best-practices-performance-patterns):** Anti-patrones SQL en BigQuery y cómo evitarlos.

## Ciencia de Datos

- **[Hidden Technical Debt in Machine Learning Systems](https://papers.nips.cc/paper/5656-hidden-technical-debt-in-machine-learning-systems.pdf):**, explica las consideraciones a tener a la hora de construir y mantener algoritmos de ML.

### Cloud

- **[Google Cloud Developer Cheat Sheet](https://docs.google.com/spreadsheets/d/1OkFbizpnc_iyzcApqRrqsNtUVazKJDtCyH5vw3352xM/htmlview?usp=drive_open&ouid=116471976024911956616&sle=true):** Todos los links de interés para manejarse en GCP.

### Tools

- **[Michelangelo PyML](https://eng.uber.com/michelangelo-pyml/):** Librería para ML en Python, usado para entrenar modelos predictivos en Uber. Hay dos versiones: una que funciona sobre Apache Spark (Michelangelo), más eficiente pero menos flexible, y una que funciona sobre Python (Michelangelo PyML), más flexible pero menos eficiente.
- **[Adanet](https://github.com/tensorflow/adanet):** Framework de ML desarrollado por Google sobre TensorFlow.
- **[lime](https://github.com/marcotcr/lime):** Explicador de algoritmos de clasificación, entendiéndose que un explicador es una aproximación lineal local del comportamiento de un modelo para entender lo que hace.
- **[Kubeflow](https://www.kubeflow.org/):** Permite deployar workflows de ML sobre Kubernetes de forma sencilla.
- **[TensorSpace Playground](https://tensorspace.org/html/playground/index.html):** Visualizaciones de modelos preconstruidos de deep neural networks.
- **[YOLO-LITE: A Real-Time Object Detection Algorithm Optimized for Non-GPU Computers](https://arxiv.org/abs/1811.05588):** Algoritmo en tiempo real de detección de objetos para computadores sin GPU.
- **[Dask](https://dask.org/):** Escala Python para que use paralelismo. Útil a la hora de entrenar modelos muy pesados.
- **[PyTorch Geometry](https://github.com/arraiy/torchgeometry):** Librería para PyTorch que permite resolver problemas de visión geométrica en computadoras.
- **[EuclidesDB](https://euclidesdb.readthedocs.io/en/latest/):** Base de datos diseñada especialmente para ML.
- **[floydhub](https://blog.floydhub.com/turning-design-mockups-into-code-with-deep-learning/):** Mockups to HTML: Crear páginas web con Deep Learning

### Ejemplos

- **[Detecting Parkinson's with XGBoost](https://towardsdatascience.com/detect-parkinsons-with-10-lines-of-code-intro-to-xgboost-51a4bf76b2e6):** Tutorial básico de weak learner.
- **[Machine Learning: del arbol de decisión al bosque de árboles con boosting (XGBoost)](https://www.youtube.com/watch?v=PN-8dTXh3DU):** Tutorial de XGBoost.
- **[Running JanusGraph with BigTable](https://cloud.google.com/solutions/running-janusgraph-with-bigtable):** Base de datos en base a grafos sobre BigTable, útil para insights.
- **[A Gentle Introduction to XGBoost for Applied Machine Learning](https://machinelearningmastery.com/gentle-introduction-xgboost-applied-machine-learning/):** Introducción teórica a XGBoost, árboles de decisión en base a descenso de gradiente.
- **[Presenting A Modern Big Data Algorithm: Hyperlearn- 50%+ Faster Machine Learning](https://www.techleer.com/articles/568-presenting-a-modern-big-data-algorithm-hyperlearn-50-faster-machine-learning/):** Algoritmos de Statsmodel optimizados en uso de recursos.
- **[A new course to teach people about fairness in machine learning](https://www.blog.google/technology/ai/new-course-teach-people-about-fairness-machine-learning/?utm_source=newsletter&utm_medium=email&utm_campaign=2018-november-gcp-newsletter-en):** Concepto de justicia en algoritmos de ML.
- **[Creating a PyTorch Deep Learning VM Instance](https://cloud.google.com/deep-learning-vm/docs/pytorch_start_instance?utm_source=newsletter&utm_medium=email&utm_campaign=2018-november-gcp-newsletter-en):** Deep learning con instancias de VM en Google Cloud.
- **[The Revenge of Neurons](https://neurovenge.antonomase.fr/):** Paper con la historia de las redes neuronales.
- **[The Tensor Product, Demystified](https://www.math3ma.com/blog/the-tensor-product-demystified):** Introducción a la matemática detrás de TensorFlow.
- **[How to Conquer Tensorphobia](https://jeremykun.com/2014/01/17/how-to-conquer-tensorphobia/):** Cómo funciona el producto cruz entre tensores en TensorFlow.
- **[Python Machine Learning (2nd Ed.) Code Repository](https://github.com/rasbt/python-machine-learning-book-2nd-edition):** Repositorio de código para el libro *Python Machine Learning*.
- **[Stochastic Deep Networks](https://arxiv.org/abs/1811.07429):** Paper que propone un deep framework para manejar aspectos cruciales de medidas en datos de entrada para redes neuronales.
- **[Understand these 4 advanced concepts to sound like a machine learning master](https://towardsdatascience.com/understand-these-4-advanced-concepts-to-sound-like-a-machine-learning-master-d32843840b52):** Conceptos de ML explicados con peras y manzanas.
- **[Doing XGBoost hyper-parameter tuning the smart way](https://medium.com/@mateini_12893/doing-xgboost-hyper-parameter-tuning-the-smart-way-part-1-of-2-f6d255a45dde):** Cómo hacer tuning de hiperparámetros en XGBoost.
- **[pair2vec: Compositional Word-Pair Embeddings for Cross-Sentence Inference](https://arxiv.org/abs/1810.08854):** Paper que describe un método para integrar razonamiento de relaciones implícitas entre pares de palabras.
- **[GpyTorch](https://github.com/cornellius-gp/gpytorch):** Librería para procesos gaussianos en PyTorch
- **[Deep Learning Recommender System](https://towardsdatascience.com/recommender-systems-with-deep-learning-architectures-1adf4eb0f7a6):** Sistema de recomendación utilizando Deep Learning
- **[Data Science PyThon notebooks](https://github.com/donnemartin/data-science-ipython-notebooks):** Compendio de buenos notebooks de data science.
- **[DL Rooftop](https://towardsdatascience.com/using-image-segmentation-to-identify-rooftops-in-low-resolution-satellite-images-c791975d91cc):** Deep Learning para detectar edificios en un Mapa
- **[Learning Demostration in The Wild](https://www.latentlogic.com/learning-from-demonstration-in-the-wild/):** Ejemplo de aprendizaje reforzado (Demostration in The Wild)
- **[Apache Spark @Scale: A 60 TB+ production use case](https://code.fb.com/core-data/apache-spark-scale-a-60-tb-production-use-case/):** Uso de Spark en Facebook para el análisis de grandes volumenes de datos 60 TB+
- 
### Series de tiempo
- **[k-Shape: Efficient and Accurate Clustering of Time Series](http://www1.cs.columbia.edu/~jopa/Papers/PaparrizosSIGMOD2015.pdf):** k-Shape, agrupamiento de series de tiempo.


### Estadística

- **[Evaluando sesgos en modelos](https://www.technologyreview.com/s/607955/inspecting-algorithms-for-bias/):** Sesgo estadístico en Modelos Analíticos.
- **[Metricas de evaluación de modelos](http://byteus.tech/comparison-between-machine-learning-evaluation-metrics/):** Métodos estadísticos para evaluar Modelos.

### Wiki
- **[Cheat Sheets AI](https://github.com/kailashahirwar/cheatsheets-ai):** Compendio de AI.
- **[Python ML Book](https://github.com/rasbt/python-machine-learning-book-2nd-edition):** Libro ML en Python

## Proyectos de Analytics

- **[Is Amazon Primed to Conquer Christmas Again?](https://www.bain.com/insights/retail-holiday-newsletter-2018-issue-2-is-amazon-primed-to-conquer-christmas-again/?utm_medium=Newsletter&utm_source=Retail-Holiday-Newsletter-2018-November-2018&utm_campaign=retail-holiday-newsletter-2018-issue-2-is-amazon-primed-to-conquer-christmas-again&mkt_tok=eyJpIjoiTkRZMFlqTTNaV1JsWlRZMSIsInQiOiJIQ3NvYndcL1JvREZYRndzZm9MZnNpOUFLV2hYbHF5U2NMdDloRlNTNm5WNElFVXVoNVY3MWJXNEFaZjdBV0JTdUhTV2tETGE2bENXM3Ridnh6OXF6TnFRd1wvZ29vNHRzbDFBMUowMUhNWVZ1clhsc2NhQkRwbHBTVE5NS2t0bHUxIn0%3D):** Tácticas de cómo Amazon busca expandir su mercado en la temporada navideña del 2018.
- **AI in Motion: designing a simple system to see, understand, and react in the real world [(parte 1)](https://cloud.google.com/blog/products/ai-machine-learning/ai-motion-designing-simple-system-see-understand-and-react-real-world-part-i) [(parte 2)](https://cloud.google.com/blog/products/ai-machine-learning/ai-motion-designing-simple-system-see-understand-and-react-real-world-part-ii) [(parte 3)](https://cloud.google.com/blog/products/ai-machine-learning/ai-in-motion-designing-a-simple-system-to-see-understand-and-react-in-the-real-world-part-iii?utm_source=newsletter&utm_medium=email&utm_campaign=2018-november-gcp-newsletter-en):** Proyecto de demostración que delinea los pasos para crear una AI que reaccione al mundo real.
- **[TensorFlow Playground](https://playground.tensorflow.org/):** Demo visual de redes neuronales en TF.
- **[How to capture and store tweets in Real Time with Apache Spark and Apache Kafka. Using cloud Platforms such as Databricks and GCP](https://towardsdatascience.com/how-to-capture-and-store-tweets-in-real-time-with-apache-spark-and-apache-kafka-e5ccd17afb32?fbclid=IwAR1PcNqmPp_CYidq-PJLWL4dGcuH4WsFTGQevSR8j5RMZ75S4OnsJnmoU50):** Ejemplo de proyecto para obtener datos desde Twitter con Kafka y pasarlos por Spark.
--**[Proyecto de Churn usando Keras](https://blogs.rstudio.com/tensorflow/posts/2018-01-11-keras-customer-churn/):** Usando Keras para atacar Churn
--**[Data Science a Escala Empresarial](https://t.co/GA3qSaR7tz):** Presentacion sobre Data Science en Empresas treadicionales

## Academia
- **[Página Victor Chernozhukov MIT](http://web.mit.edu/~vchern/www/):** Lista de publicaciones y otros.
- **[Map of knowledge](https://arxiv.org/abs/1811.07974):** Paper sobre clasificaciones analíticas del conocimiento.
- **[Map of Complexity Sciences](http://www.art-sciencefactory.com/complexity-map_feb09.html):** Diagrama con relaciones entre las ciencias del estudio de la complejidad.
- **[Mastering Apache Spark GitBook](https://jaceklaskowski.gitbooks.io/mastering-apache-spark/):** GitBook para el estudio de Apache Spark, con correctas practicas de programación.
- **[The Internals of Spark Structured Streaming](https://jaceklaskowski.gitbooks.io/spark-structured-streaming/):** GitBook para el estudio de Streaming en Apache Spark.
- **[Mastering Kafka Streams 2.0.0](https://jaceklaskowski.gitbooks.io/mastering-kafka-streams/):** GitBook para el estudio de Apache Kafka.
- **[Papers with Code](https://www.paperswithcode.com/):** Compendio de Papers con Código
- **[Style Transfer](https://arxiv.org/abs/1508.06576?fbclid=IwAR2oUPT9Uk_LP9IqWMxhITSVvgtomvTsKMf9_dgN2sisCWnCSMK44RK9s-Y):** Transferencia de estilos (DLArt)

## Gestión AA & Big Data:
- **[Agile Data Science 2.0 by Russell Jurney](https://www.oreilly.com/library/view/agile-data-science/9781491960103/ch01.html):** "Agile Data Science" es un acercamiento a la Ciencia de datos centrada en el desarrollo de aplicaciones web.

## Otros
- **[Abacus-Util](https://github.com/landawn/AbacusUtil):** Librería en Java para programación general, avance para lidear con típicos problemas de data wrangling
- **[Scala Exercises](https://www.scala-exercises.org/):** Variedad de ejercicios en Scala.
- **[Scala Koans](http://www.scalakoans.org/):** Pequeños ejercicios para motivar el aprendizaje en base prueba y error en Scala.
- **[NLP](https://github.com/RubensZimbres/Repo-2017/):** Repositorio de NLP.

## Contribuidores

Por orden de enrolamiento

- **[Karim Touma](https://www.linkedin.com/in/karimop/)**
- **[Matías Cortés](https://www.linkedin.com/in/mat%C3%ADas-cort%C3%A9s-9a8962160/)**
- **[Jesús Méndez](https://www.linkedin.com/in/jmendezgal/)**
- **[Rodolfo Maripán](https://www.linkedin.com/in/rodolfo-maripan-66198654/)**
- **[Carlos Vergara](https://www.linkedin.com/in/carlosvergarapadilla/)** 
